﻿class Program
{
    static void Main(string[] args)
    {
        // Create a new business
        Business businessOne = new Business("Cottingham & Butler");

        // Create recruiters
        Recruiter recruiter1 = new Recruiter("David Jackman", "Senior Engineer Recruiter", 10);
        Recruiter recruiter2 = new Recruiter("Casey Kieler", "Senior Engineer Recruiter", 2);
        Recruiter recruiter3 = new Recruiter("McKenzie Kline", "Senior Talent Acquisition Manager", 5);
        Recruiter recruiter4 = new Recruiter("John Stevenson", "Senior Insurance Claim Recruiter", 8);
        Recruiter recruiter5 = new Recruiter("Kelly Kapowski", "Senior Project Manager", 8);


        // Announce recruiters background
        businessOne.AnnounceRecruiter(recruiter1);
        businessOne.AnnounceRecruiter(recruiter2);
        businessOne.AnnounceRecruiter(recruiter3);
        businessOne.AnnounceRecruiter(recruiter4);
        businessOne.AnnounceRecruiter(recruiter5);

        // Create recruits
        Recruit recruit1 = new Recruit("Zach Dempsey", "Software Developer", 1);
        Recruit recruit2 = new Recruit("Sarah Morett", "Insurance Claims Adjuster", 3);
        Recruit recruit3 = new Recruit("Adam Adam", "Assistant Project Manager", 2);

        // Next Steps
        recruiter3.NextSteps(recruit1, recruiter1, recruiter2);
        recruiter3.NextSteps(recruit2, recruiter4);

        // Hire recruits
        recruiter1.HireRecruit(recruit1, recruiter2);
        recruiter4.HireRecruit(recruit2);

        // Deny recruits
        recruiter3.DenyRecruit(recruit3);
        
        // Print the list of recruiters
        businessOne.PrintRecruiters();

    }
}
