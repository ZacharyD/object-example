class Recruiter
{
    public string Name { get; set; }
    public string Position { get; set; }
    public int Experience { get; set; }

    public Recruiter(string name, string position, int experience)
    {
        Name = name;
        Position = position;
        Experience = experience;
    }

    public virtual void NextSteps(Recruit recruit, Recruiter recruiterTwo)
    {
        Console.WriteLine($"{recruit.Name}, your phone interview with {Name} went well and we would like you to meet with our {recruiterTwo.Position}, {recruiterTwo.Name}. You will be contacted shortly with further information.");
    }
    public void NextSteps(Recruit recruit, Recruiter recruiterTwo, Recruiter recruiterThree)
    {
        Console.WriteLine($"{recruit.Name}, your phone interview with {Name} went well and we would like you to meet with our {recruiterTwo.Position}, {recruiterTwo.Name} and {recruiterThree.Position}, {recruiterThree.Name}. You will be contacted shortly with further information.");
    }
    public virtual void HireRecruit(Recruit recruit)
    {
        Console.WriteLine($"Recruit {recruit.Name} has been hired as a {recruit.Position} by {Name}.");
    }

    public void HireRecruit(Recruit recruit, Recruiter recruiterTwo)
    {
        Console.WriteLine($"Recruit {recruit.Name} has been hired as a {recruit.Position} by {Name} and {recruiterTwo.Name}.");
    }

    public void DenyRecruit(Recruit recruit)
    {
        Console.WriteLine($"{recruit.Name}, We regret to inform you that we have already filled the position(s) for {recruit.Position} and are currently not seeking additional team members for this role.");
    }

}
