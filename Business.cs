
class Business
{
    public string Name { get; set; }
    public List<Recruiter> Recruiters { get; set; }

    public Business(string name)
    {
        Name = name;
        Recruiters = new List<Recruiter>();
    }

    public void AnnounceRecruiter(Recruiter recruiter)
    {
        Recruiters.Add(recruiter);
        Console.WriteLine($"{recruiter.Name} has worked at {Name} for {recruiter.Experience} years.");
    }

    public void PrintRecruiters()
    {
        Console.WriteLine($"Recruiters at {Name}:");
        foreach (var recruiter in Recruiters)
        {
            Console.WriteLine($"{recruiter.Name} - {recruiter.Position}");
        }
    }
}
