class Recruit
{
    public string Name { get; set; }
    public string Position { get; set; }
    public int Experience { get; set; }

    public Recruit(string name, string position, int experience)
    {
        Name = name;
        Position = position;
        Experience = experience;
    }
}
